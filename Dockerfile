FROM busybox

ADD data /data

VOLUME ["/data/"]

CMD ["/bin/sh"]
